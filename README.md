<a name="top"></a>

<div align="center">
<img src="https://i.imgur.com/4QxMBJ3.png" >
</div>
<h6 href="#top" display="none"></h6>


### This is an ongoing workplace where I can show some of the interesting things I learned using Unity engine.

## About the project

This project was made using [Unity 2020.3.33f1](https://unity3d.com/es/unity/whats-new/2020.3.33) exploring the possibilities of shaders, VFX, particle systems and render features in this engine. I am creating as I learn and learning as I create, constantly tying to push myself off the confort zone of doing what I already understand how.

## Getting Started

To get your hands on the project I strongly recommend to download the unity version linked above. Previous versions may not work as expected. Shader graph and VFX graph codebases change between versions and may cose internal errors.

Once you clone it, you will find the museum scene, everything the project haves is exposed there in some form. Except some scripts of procedural terrain generation that I am currently working on.

In the Assets folder its everything divided by its type of function.

Here is a [notion site](https://argo3d.notion.site/Unity-Shader-Notes-aa0625ce07d44a0485376527c620a5e7) where I document the effects exposed in the scene - Work-in-progress.


Feel free to contact me if any issue comes up.

## Usage

This project is an interactive show-reel, feel free to explore the scene and looking how everything was build and tweaking values. Since there are a lot of elements in the project assets folder, I recommend looking the elements of each exposition by the inspector. Clicking what to see from the references holded in the inspector.

In the near future I will provide a description and walk-through of the shaders and effects that are currently in the Museum.

   <br>

Play the project: [My_Shader_Museum](https://argogames.itch.io/my-shader-museum)

## License

Distributed under the CCO License. See `LICENSE.txt` for more information.


## Contact

Raimundo Gallino (ARGO) - [web](https://www.argo3d.xyz) - [Linkedin](https://www.linkedin.com/in/raimundo-gallino/)


<p text-align="right"><a href="#readme-top">Back to top</a></p>


