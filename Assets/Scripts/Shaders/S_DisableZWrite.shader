Shader "Custom/S_DisableZWrite"
{
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        pass { ZWrite Off }
    }
}
