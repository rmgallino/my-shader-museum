using System.Collections;
using StarterAssets;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    [Header("UI")]
    public static bool isGamePaused = true;
    public static bool startGame = true;
    public PlayerInput playerInput;
    public GameObject UI;
    public GameObject player;
    public GameObject blurUI;
    public TextMeshProUGUI textComponent;
    public RectTransform resumeButton, exitButton, creditsButton, title;
    public CanvasGroup backgroundCanvasGroup, menuButtonsCanvasGroup, creditsButtonsCanvasGroup, creditsTextCanvasGroup;
    public GameObject menuButtonsGroup, creditsButtonGroup, creditsTextGroup;

    [Header("Animation Values")] 
    public float fadeTime = 1f;
    
    [Header("Rendering Pipeline")]
    public UniversalRenderPipelineAsset gameAsset;
    public UniversalRenderPipelineAsset pauseMenuAsset;
    

    private void Start()
    {
        UI.SetActive(true);
        Cursor.visible = true;
        GraphicsSettings.renderPipelineAsset = pauseMenuAsset;
        QualitySettings.renderPipeline = pauseMenuAsset;
        
    }

    public void PressPause()
    {
        if (startGame) return;
        UI.SetActive(isGamePaused);
        UnhideCursor();
        if (isGamePaused)
        {
            player.GetComponent<FirstPersonController>().enabled = false;
            GraphicsSettings.renderPipelineAsset = pauseMenuAsset;
            QualitySettings.renderPipeline = pauseMenuAsset;
            UnhideCursor();
            // Debug.Log("Default render pipeline asset is: " + GraphicsSettings.renderPipelineAsset.name);
        }
        else
        {
            player.GetComponent<FirstPersonController>().enabled = true;
            GraphicsSettings.renderPipelineAsset = gameAsset;
            QualitySettings.renderPipeline = gameAsset;
            HideCursor();
            // Debug.Log("Default render pipeline asset is: " + GraphicsSettings.renderPipelineAsset.name);
        }
        isGamePaused = !isGamePaused;
        
        // TODO: doble press escape issue
    }

    public void PressStart()
    {
        startGame = false;
        UI.SetActive(false);
        HideCursor();
        GraphicsSettings.renderPipelineAsset = gameAsset;
        QualitySettings.renderPipeline = gameAsset;
    }

    public void PressResume()
    {
        player.GetComponent<FirstPersonController>().enabled = true;
        GraphicsSettings.renderPipelineAsset = gameAsset;
        QualitySettings.renderPipeline = gameAsset;
        
        UI.SetActive(false);
        HideCursor();
    }

    public void PressCredits()
    {
        startGame = false;
        PanelFadeIn();
        StartCoroutine("CreditsAnimationIn");
    }

    public void PressBackFromCredits()
    {
        startGame = false;
        PanelFadeOut();
        StartCoroutine("CreditsAnimationOut");

    }

    public void PressExit()
    {
        Application.Quit();
    }

    // aux functions
    private void PanelFadeIn()
    {
        backgroundCanvasGroup.alpha = 0f;
        backgroundCanvasGroup.DOFade(1, fadeTime);
        menuButtonsCanvasGroup.alpha = 1f;
        menuButtonsCanvasGroup.DOFade(0, fadeTime);
        menuButtonsGroup.SetActive(false);
    }
    private void PanelFadeOut()
    {
        backgroundCanvasGroup.alpha = 1f;
        backgroundCanvasGroup.DOFade(0, fadeTime);
        menuButtonsCanvasGroup.alpha = 0f;
        menuButtonsCanvasGroup.DOFade(1, fadeTime);
        menuButtonsGroup.SetActive(true);

    }

    IEnumerator CreditsAnimationIn()
    {
        var titlePos = title.transform.position;
        var creditsButtonsPos = creditsButtonGroup.transform.position;
        var creditsTextPos = creditsTextGroup.transform.position;

        
        // Title
        title.DOMoveY(titlePos.y + 0.18f,2,false).SetEase(Ease.OutExpo);
        
        yield return new WaitForSeconds(1);
        
        // Text
        creditsButtonsCanvasGroup.alpha = 0f;
        creditsTextCanvasGroup.DOFade(1, 0.5f);
        creditsTextGroup.transform.DOMoveY(creditsTextPos.y + 0.005f, 0.5f).SetEase(Ease.InOutSine);

        
        yield return new WaitForSeconds(1f);
        
        // Credits buttons
        creditsButtonGroup.SetActive(true);
        creditsButtonsCanvasGroup.alpha = 0f;
        creditsButtonsCanvasGroup.DOFade(1, fadeTime);
        creditsButtonGroup.transform.DOMoveY(creditsButtonsPos.y + 0.1f, 1, false).SetEase(Ease.InOutSine);
            
        yield return null;
    }

    IEnumerator CreditsAnimationOut()
    {
        var titlePos = title.transform.position;
        var creditsButtonsPos = creditsButtonGroup.transform.position;
        var creditsTextPos = creditsTextGroup.transform.position;

        // Title
        title.DOMoveY(titlePos.y - 0.18f,2,false).SetEase(Ease.OutExpo);
        
        // Text
        creditsButtonsCanvasGroup.alpha = 1f;
        creditsTextCanvasGroup.DOFade(0, 0.5f);
        creditsTextGroup.transform.DOMoveY(creditsTextPos.y - 0.005f, 0.5f).SetEase(Ease.InOutSine);
        // Credits buttons
        creditsButtonGroup.SetActive(true);
        creditsButtonsCanvasGroup.alpha = 1f;
        creditsButtonsCanvasGroup.DOFade(0, fadeTime);
        creditsButtonGroup.transform.DOMoveY(creditsButtonsPos.y - 0.1f, 0.5f, false).SetEase(Ease.InOutSine);
            
        yield return null;
    }
    
    public void HideCursor()
    {
        if (Cursor.visible)
            Cursor.visible = false;
    }

    public void UnhideCursor()
    {
        if (!Cursor.visible)
            Cursor.visible = true;
    }


}
