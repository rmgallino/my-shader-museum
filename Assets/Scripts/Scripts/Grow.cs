using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grow : MonoBehaviour
{
    public List<MeshRenderer> growVineMeshes;
    public float timeToGrow = 5;
    public float refreshRate = 0.5f;
    [Range(0, 1)] 
    public float minGrow = 0;
    [Range(0, 1)] 
    public float maxGrow = 1;

    private List<Material> _growVinesMaterials = new List<Material>();
    private bool fullyGrown;
    
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < growVineMeshes.Count; i++)
        {
            for (int j = 0; j < growVineMeshes[i].materials.Length; j++)
            {
                if (growVineMeshes[i].materials[j].HasProperty("_Grow"))
                {
                    growVineMeshes[i].materials[j].SetFloat("_Grow", minGrow);
                    _growVinesMaterials.Add(growVineMeshes[i].materials[j]);
                }
            }
        }
        
    }

    // Update is called once per frame
    void LateUpdate()
    { 
        for (int i = 0; i < _growVinesMaterials.Count; i++)
        {
            StartCoroutine(GrowVines (_growVinesMaterials[i]));       
        }
    }

    IEnumerator GrowVines (Material mat)
    {
        float growValue = mat.GetFloat("_Grow");

        while (true)
        {
            if (!fullyGrown)
            {
                while (growValue < maxGrow)
                {
                    growValue += 1 / (timeToGrow / refreshRate);
                    mat.SetFloat("_Grow", growValue);

                    yield return new WaitForSeconds(refreshRate);
                }
            }
            else
            {
                while (growValue > minGrow)
                {
                    growValue -= 1 / (timeToGrow / refreshRate);
                    mat.SetFloat("_Grow", growValue);

                    yield return new WaitForSeconds(refreshRate);
                }
            }

            if (growValue >= maxGrow)
            {
                fullyGrown = true;
            }
            else
            {
                fullyGrown = false;
            }
        }
    }
}
