using System.Collections;
using UnityEngine;

public class GenerateGhosts : MonoBehaviour
{
    public Transform posToSpawn;
    public SkinnedMeshRenderer skmr;
    
    public float lifetime = 1.0f;
    public float repeatDelay = 0.4f;
    public float disappearTime = 0.3f;

    private SkinnedMeshRenderer[] _skinnedMeshRenderers;
    private Mesh mesh;
    
    private void Start()
    {
        InvokeRepeating("StartTrail", lifetime, repeatDelay);
        mesh = new Mesh();
    }

    void StartTrail()
    {
        GameObject ghost = GeneratePool.SharedInstance.GetPooledObject();
        
        if (ghost != null) {
            
            var t = posToSpawn.transform;
            ghost.transform.position = t.position;
            ghost.transform.rotation = t.rotation;
            ghost.transform.localScale = t.localScale;

            MeshFilter mf = ghost.GetComponent<MeshFilter>();
            MeshRenderer mr = ghost.GetComponent<MeshRenderer>();
                
            // Reuse previous mesh
            skmr.BakeMesh(mf.mesh);
            
            ghost.SetActive(true);
            StartCoroutine(GhostLife(ghost, mr.material, mf.mesh));
        }
        
    }
    IEnumerator GhostLife(GameObject ghost, Material mat, Mesh mesh)
    {
        float time = 0;
        var alpha = 1f;

        while (time < lifetime)
        {
            // Material
            alpha = Mathf.Lerp(1,0,time / disappearTime);
            
            mat.SetFloat("_Alpha", alpha);
            
            yield return null;
            time += Time.deltaTime;
        }
        // Reset
        ghost.SetActive(false);
        mat.SetFloat("_Alpha", 1);
        mesh.Clear();
    }
}