using System.Collections.Generic;
using UnityEngine;

public class GeneratePool : MonoBehaviour
{
    // other scripts will need to access the object pool during gameplay,
    // and public static instance allows other scripts to access it without getting a Component from a GameObject.
    public static GeneratePool SharedInstance;
    void Awake() {
        SharedInstance = this;
    }

    public int ghostAmount = 10;
    public Material mat;
    List<GameObject> ghostPool = new List<GameObject>();
    
    void Start()
    {
        for (int i = 0; i < ghostAmount; i++)
        {
            GameObject obj = new GameObject("Trail" + i);
            obj.transform.position = transform.position;
            obj.transform.rotation = transform.rotation;
            obj.transform.SetParent(transform);
            
            MeshRenderer mr = obj.AddComponent<MeshRenderer>();
            MeshFilter mf = obj.AddComponent<MeshFilter>();
            
            mr.material = mat;
            mf.mesh = new Mesh();
            
            obj.SetActive(false);
            ghostPool.Add(obj);
        }
    }

    public GameObject GetPooledObject() {
        
        for (int i = 0; i < ghostPool.Count; i++) {

            if (!ghostPool[i].activeInHierarchy) {
                return ghostPool[i];
            }
        }
        return null;
    }
}
