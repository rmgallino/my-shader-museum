using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshZoneWandering : MonoBehaviour
{
    [SerializeField] private NavMeshAgentController[] agents;

    private Dictionary<string, ZoneData> zonesDict = new Dictionary<string, ZoneData>();
    [SerializeField] private ZoneData[] zonesData;
    [SerializeField] private string zoneIndex;
    private ZoneData currentZone;
    [Space] [SerializeField] private bool loop = false;
    [SerializeField] private bool playOnAwake;
    [SerializeField] private RandomData newPosDelay;

    private void Start()
    {
        foreach (var zoneData in zonesData)
            zonesDict.Add(zoneData.Name, zoneData);

        agents = GetComponentsInChildren<NavMeshAgentController>(); 
        MoveZone(zoneIndex); 
        SetLoop(loop);

        if (playOnAwake)
            MoveToZone();
    }
    
    public void MoveToZone()
    {
        StopAllCoroutines();
        foreach (var agent in agents) {
            OnReachedtarget(agent);
        }
    }
    
    public void SetLoop(bool loopValue)
    {
        loop = loopValue;
        if (loopValue) {
            foreach (var agent in agents)
                agent.OnTargetReached.AddListener(OnReachedtarget);
        }
        else {
            foreach (var agent in agents)
                agent.OnTargetReached.RemoveListener(OnReachedtarget);
        }
    }

    private void OnReachedtarget(NavMeshAgentController agent) => StartCoroutine(NewPosDelay(agent));
    IEnumerator NewPosDelay(NavMeshAgentController agent)
    {
        yield return new WaitForSeconds(newPosDelay.GetRadom);
        var position = GetRandomPointInsideCollider(currentZone.Zone);
        agent.SetTarget(RandomNavmeshLocation(position, currentZone.Zone.size.magnitude));
    }

    public void MoveZone(string index)
    {
        if (!zonesDict.ContainsKey(index))
        {
            Debug.LogError($"Zones not contain key: {index}");
            return;
        }
        currentZone = zonesDict[index];
    }
    
    private Vector3 GetRandomPointInsideCollider(BoxCollider boxCollider)
    {
        Vector3 extents = boxCollider.size / 2f;
        Vector3 point = new Vector3(
            Random.Range(-extents.x, extents.x),
            Random.Range(-extents.y, extents.y),
            Random.Range(-extents.z, extents.z)
        ) + boxCollider.center;
        return boxCollider.transform.TransformPoint(point);
    }
    
    private static Vector3 RandomNavmeshLocation(Vector3 pos, float radius)
    {
        NavMeshHit hit;
        NavMesh.SamplePosition(pos, out hit, radius, 1);
        return hit.position;
    }

    [System.Serializable]
    class ZoneData
    {
        public string Name;
        public BoxCollider Zone;
    }
}
