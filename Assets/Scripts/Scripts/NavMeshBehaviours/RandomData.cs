﻿using UnityEngine;

[System.Serializable]
class RandomData
{
    [SerializeField] private float minValue;
    [SerializeField] private float maxValue;
    public float GetRadom => Random.Range(minValue, maxValue);
}
