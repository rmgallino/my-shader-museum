using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[RequireComponent(typeof(NavMeshAgent))]
public class NavMeshAgentController : MonoBehaviour
{
    public Transform Target { get; private set; }

    float a;
    public float Speed
    {
        set => myAgent.speed = value;
    }

    private NavMeshAgent myAgent;
    public NavMeshAgent Agent => myAgent;

    [SerializeField] private float stopTolerance = 2; 
    
    [SerializeField] private UnityEvent onStartTarget;
    public UnityEvent OnStartTarget => onStartTarget;
    
    [SerializeField] private UnityEvent<NavMeshAgentController> onTargetReached;
    public UnityEvent<NavMeshAgentController> OnTargetReached => onTargetReached;

    private Coroutine chasingTargetRoutine;
    
    public Vector3 Velocity => myAgent.velocity;

    private void Awake()
    {
        myAgent = GetComponent<NavMeshAgent>();
    }

    public void SetTarget(Transform target)
    {
        Target = target;
        
        if (chasingTargetRoutine != null)
            StopCoroutine(chasingTargetRoutine);
        
        onStartTarget.Invoke();
        chasingTargetRoutine = StartCoroutine(GoToTarget());
    }
    
    public void SetTarget(Vector3 pos)
    {
        if (chasingTargetRoutine != null)
            StopCoroutine(chasingTargetRoutine);
        
        onStartTarget.Invoke();
        chasingTargetRoutine = StartCoroutine(GoToTarget(pos));
    }

    private IEnumerator GoToTarget()
    {
        myAgent.SetDestination(Target.position);
        yield return new WaitForSeconds(stopTolerance);
        
        while (myAgent.velocity != Vector3.zero) {
            myAgent.SetDestination(Target.position);
            yield return null;
        }
        
        onTargetReached.Invoke(this);
    }
    
    private IEnumerator GoToTarget(Vector3 pos)
    {
        myAgent.SetDestination(pos);
        yield return new WaitForSeconds(stopTolerance);
        
        // Wait till the Agent is stopped
        while (myAgent.velocity != Vector3.zero) {
            yield return null;
        }
        
        onTargetReached.Invoke(this);
    }
}
