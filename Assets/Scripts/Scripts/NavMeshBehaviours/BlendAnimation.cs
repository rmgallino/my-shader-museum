using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendAnimation : MonoBehaviour
{

    public Animator animator;
    private float velocity;
    private Vector3 mLastPosition;
    

    // Update is called once per frame
    void Update()
    {
        float elapsedTime = 1;
        var position = transform.position;
        float speed = (position - this.mLastPosition).magnitude / elapsedTime;
        mLastPosition = position;
        animator.SetFloat("Velocity", speed);
    }
}
