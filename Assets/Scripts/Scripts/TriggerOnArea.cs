using UnityEngine;

public class TriggerOnArea : MonoBehaviour
{
    public GameObject objectToActivate;
    private void Start()
    {
        objectToActivate.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        objectToActivate.gameObject.SetActive(true);
    }
}
