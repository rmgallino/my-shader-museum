using DG.Tweening;
using UnityEngine;

public class LoopAnimate : MonoBehaviour
{
    [SerializeField] private float endValueMoveY;
    [SerializeField] private float endValueMoveX;
    [SerializeField] private float endValueMoveZ;
    [SerializeField] private float endValueRotY;
    
    [SerializeField] [Range(0, 10)] private float duration;
    
    void Start()
    {
        DOTween.Init();


        Vector3 move = new Vector3(endValueMoveX, endValueMoveY, endValueMoveZ);
        
        transform.DOMove(move, duration)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.InOutSine)
            .SetRelative();

        transform.DORotate(new Vector3(0, endValueRotY, 0), duration, RotateMode.FastBeyond360)
            .SetEase(Ease.Linear)
            .SetLoops(-1)
            .SetRelative();
    }
}
